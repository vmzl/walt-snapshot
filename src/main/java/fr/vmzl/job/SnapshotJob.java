package fr.vmzl.job;

import fr.vmzl.domain.Wallet;
import fr.vmzl.service.AddressAPIService;
import fr.vmzl.service.WalletService;
import fr.vmzl.service.impl.BinanceAPIServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

public class SnapshotJob {
    @Autowired
    WalletService walletService;

    AddressAPIService addressAPIService;

    void runDailySnapshot(Wallet wallet){
        //switch (wallet.getS)
        addressAPIService = new BinanceAPIServiceImpl(wallet);
        addressAPIService.createWalletSnapshot();
    }

    public void dailySnapshot(){
        for(Wallet wallet : walletService.findAll()){
            runDailySnapshot(wallet);
        }
    }
}
