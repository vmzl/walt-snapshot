package fr.vmzl.repository;

import fr.vmzl.domain.Crypto;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Crypto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long> {}
