package fr.vmzl.service;

import fr.vmzl.domain.Crypto;


public interface AddressAPIService {

    Double getPrice(Crypto crypto);

    void createRecord(Crypto crypto, Double quantity);

    void createWalletSnapshot();

}
