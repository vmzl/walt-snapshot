package fr.vmzl.service.impl;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.Account;
import com.binance.api.client.domain.account.AssetBalance;
import com.binance.api.client.domain.market.TickerStatistics;
import com.binance.api.client.exception.BinanceApiException;
import fr.vmzl.domain.Crypto;
import fr.vmzl.domain.Record;
import fr.vmzl.domain.Wallet;
import fr.vmzl.service.AddressAPIService;
import fr.vmzl.service.CryptoService;
import fr.vmzl.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinanceAPIServiceImpl implements AddressAPIService {
    private final Logger log = LoggerFactory.getLogger(BinanceAPIServiceImpl.class);

    private BinanceApiRestClient client;
    private Wallet wallet;

    @Autowired
    private WalletService walletService;
    @Autowired
    private CryptoService cryptoService;

    public BinanceAPIServiceImpl(Wallet wallet) {
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("Q0HfKdFIZlHLYF2hddCufk7lqlZEg8Sz1zeWjH3bYHftNgcaTcZrpgeatewNUUF5", "6UAGjA3fypSkUqSjg17YlYpTE0ac6YW38saUSYSVYXGfz7Z1INF49hHyGi18pcLV");
        this.client = factory.newRestClient();
        this.wallet = wallet;
    }

    @Override
    public Double getPrice(Crypto crypto) {
        TickerStatistics tickerStatistics;
        Double price = null;
        List<String> quotes = Arrays.asList("BUSD", "USDT", "BTC");
        for(String quote : quotes){
            try {
                tickerStatistics = client.get24HrPriceStatistics(crypto.getSymbol()+quote);
                price = Double.parseDouble(tickerStatistics.getLastPrice());
            }catch (BinanceApiException e){
                log.debug("Pair "+crypto.getSymbol()+"/"+quote+" not found. Let's try another one");
            }
            if(price!=null){
                log.debug("Pair "+crypto.getSymbol()+"/"+quote+" found !");
                break;
            }
        }
        return price = price!=null?price:0.0;
    }

    @Override
    public void createRecord(Crypto crypto, Double quantity) {
        Record record = new Record();
        record.setCrypto(crypto);
        record.setWallet(this.wallet);
        record.setQuantity(quantity);
        record.setDate(LocalDate.now());
        record.setPriceUSD(getPrice(crypto));
    }

    @Override
    public void createWalletSnapshot() {
        Account account = client.getAccount();
        List<AssetBalance> balances = account.getBalances();
        for(AssetBalance balance : balances){
            Crypto crypto = new Crypto();
            crypto.setSymbol(balance.getAsset());
            cryptoService.save(crypto);
            //TODO add les crypto au wallet
            createRecord(crypto, Double.parseDouble(balance.getFree())+Double.parseDouble(balance.getLocked()));
        }
        log.info("Snapshot done for wallet "+wallet.toString());
    }
}
