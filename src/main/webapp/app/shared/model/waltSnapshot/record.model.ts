import { Moment } from 'moment';
import { ICrypto } from 'app/shared/model/waltSnapshot/crypto.model';
import { IWallet } from 'app/shared/model/waltSnapshot/wallet.model';

export interface IRecord {
  id?: number;
  date?: string;
  priceUSD?: number;
  quantity?: number;
  crypto?: ICrypto;
  wallet?: IWallet;
}

export const defaultValue: Readonly<IRecord> = {};
