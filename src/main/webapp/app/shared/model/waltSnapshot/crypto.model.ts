export interface ICrypto {
  id?: number;
  symbol?: string;
  name?: string;
  imageContentType?: string;
  image?: any;
}

export const defaultValue: Readonly<ICrypto> = {};
