import { IRecord } from 'app/shared/model/waltSnapshot/record.model';

export interface IWallet {
  id?: number;
  name?: string;
  source?: string;
  address?: string;
  records?: IRecord[];
}

export const defaultValue: Readonly<IWallet> = {};
