import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './crypto.reducer';
import { ICrypto } from 'app/shared/model/waltSnapshot/crypto.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICryptoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CryptoDetail = (props: ICryptoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { cryptoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Crypto [<b>{cryptoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="symbol">Symbol</span>
          </dt>
          <dd>{cryptoEntity.symbol}</dd>
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{cryptoEntity.name}</dd>
          <dt>
            <span id="image">Image</span>
          </dt>
          <dd>
            {cryptoEntity.image ? (
              <div>
                {cryptoEntity.imageContentType ? (
                  <a onClick={openFile(cryptoEntity.imageContentType, cryptoEntity.image)}>
                    <img src={`data:${cryptoEntity.imageContentType};base64,${cryptoEntity.image}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {cryptoEntity.imageContentType}, {byteSize(cryptoEntity.image)}
                </span>
              </div>
            ) : null}
          </dd>
        </dl>
        <Button tag={Link} to="/crypto" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/crypto/${cryptoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ crypto }: IRootState) => ({
  cryptoEntity: crypto.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CryptoDetail);
