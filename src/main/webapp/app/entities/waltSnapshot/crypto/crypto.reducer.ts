import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICrypto, defaultValue } from 'app/shared/model/waltSnapshot/crypto.model';

export const ACTION_TYPES = {
  FETCH_CRYPTO_LIST: 'crypto/FETCH_CRYPTO_LIST',
  FETCH_CRYPTO: 'crypto/FETCH_CRYPTO',
  CREATE_CRYPTO: 'crypto/CREATE_CRYPTO',
  UPDATE_CRYPTO: 'crypto/UPDATE_CRYPTO',
  DELETE_CRYPTO: 'crypto/DELETE_CRYPTO',
  SET_BLOB: 'crypto/SET_BLOB',
  RESET: 'crypto/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICrypto>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type CryptoState = Readonly<typeof initialState>;

// Reducer

export default (state: CryptoState = initialState, action): CryptoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CRYPTO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CRYPTO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CRYPTO):
    case REQUEST(ACTION_TYPES.UPDATE_CRYPTO):
    case REQUEST(ACTION_TYPES.DELETE_CRYPTO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CRYPTO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CRYPTO):
    case FAILURE(ACTION_TYPES.CREATE_CRYPTO):
    case FAILURE(ACTION_TYPES.UPDATE_CRYPTO):
    case FAILURE(ACTION_TYPES.DELETE_CRYPTO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CRYPTO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CRYPTO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CRYPTO):
    case SUCCESS(ACTION_TYPES.UPDATE_CRYPTO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CRYPTO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/cryptos';

// Actions

export const getEntities: ICrudGetAllAction<ICrypto> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CRYPTO_LIST,
  payload: axios.get<ICrypto>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ICrypto> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CRYPTO,
    payload: axios.get<ICrypto>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ICrypto> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CRYPTO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICrypto> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CRYPTO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICrypto> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CRYPTO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
