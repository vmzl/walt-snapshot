import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { openFile, byteSize, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './crypto.reducer';
import { ICrypto } from 'app/shared/model/waltSnapshot/crypto.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICryptoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Crypto = (props: ICryptoProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { cryptoList, match, loading } = props;
  return (
    <div>
      <h2 id="crypto-heading">
        Cryptos
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Crypto
        </Link>
      </h2>
      <div className="table-responsive">
        {cryptoList && cryptoList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Symbol</th>
                <th>Name</th>
                <th>Image</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {cryptoList.map((crypto, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${crypto.id}`} color="link" size="sm">
                      {crypto.id}
                    </Button>
                  </td>
                  <td>{crypto.symbol}</td>
                  <td>{crypto.name}</td>
                  <td>
                    {crypto.image ? (
                      <div>
                        {crypto.imageContentType ? (
                          <a onClick={openFile(crypto.imageContentType, crypto.image)}>
                            <img src={`data:${crypto.imageContentType};base64,${crypto.image}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {crypto.imageContentType}, {byteSize(crypto.image)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${crypto.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${crypto.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${crypto.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Cryptos found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ crypto }: IRootState) => ({
  cryptoList: crypto.entities,
  loading: crypto.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Crypto);
