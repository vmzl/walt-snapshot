import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Crypto from './crypto';
import CryptoDetail from './crypto-detail';
import CryptoUpdate from './crypto-update';
import CryptoDeleteDialog from './crypto-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CryptoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CryptoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CryptoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Crypto} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={CryptoDeleteDialog} />
  </>
);

export default Routes;
