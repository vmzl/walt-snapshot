import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './record.reducer';
import { IRecord } from 'app/shared/model/waltSnapshot/record.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRecordDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RecordDetail = (props: IRecordDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { recordEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Record [<b>{recordEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="date">Date</span>
          </dt>
          <dd>{recordEntity.date ? <TextFormat value={recordEntity.date} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="priceUSD">Price USD</span>
          </dt>
          <dd>{recordEntity.priceUSD}</dd>
          <dt>
            <span id="quantity">Quantity</span>
          </dt>
          <dd>{recordEntity.quantity}</dd>
          <dt>Crypto</dt>
          <dd>{recordEntity.crypto ? recordEntity.crypto.symbol : ''}</dd>
          <dt>Wallet</dt>
          <dd>{recordEntity.wallet ? recordEntity.wallet.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/record" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/record/${recordEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ record }: IRootState) => ({
  recordEntity: record.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RecordDetail);
