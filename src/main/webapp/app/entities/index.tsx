import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Crypto from './waltSnapshot/crypto';
import Wallet from './waltSnapshot/wallet';
import Record from './waltSnapshot/record';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}crypto`} component={Crypto} />
      <ErrorBoundaryRoute path={`${match.url}wallet`} component={Wallet} />
      <ErrorBoundaryRoute path={`${match.url}record`} component={Record} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
